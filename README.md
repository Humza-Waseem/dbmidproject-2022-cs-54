# DBMIDProject-2022-CS-54

<h2>Case Study:</h2>
  Department of Computer Science UET Lahore follows the Outcome Based Education where each subject is mapped with multiple CLOs. For the Lab work, these CLOs are further mapped to multiple rubrics. Rubrics are the rules that measure the students at different levels in particular component of an assessment. 


  I am supposed to streamline the above process and develop desktop application(Windows Form Application using C#) that will be operated by the teacher to manage data at one place. Following features will be implemented in the application. <BR>
• Manage Students <BR>
• Manage CLOs <BR>
• Manage Rubrics <BR>
• Manage Assessments <BR>
• Manage Rubric Levels <BR>
• Mark the evaluations against a student        <BR>AND any other feature that can be helpful for the management of evaluations. 
Instructor also requires multiple reports in pdf form that may include. <BR>
• CLO wise class result <BR>
• Assessment wise class result
